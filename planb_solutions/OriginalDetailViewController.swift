//
//  OriginalDetailViewController.swift
//  planb_solutions
//
//  Created by 大島勇人 on 2019/01/22.
//  Copyright © 2019 Yuto Oshima. All rights reserved.
//

// WebViewを使う際はWebKitをimportする必要がある
import UIKit
import WebKit

class OriginalDetailViewController: UIViewController {
    
    // Outlet接続をしている
    @IBOutlet var webView: WKWebView!
    @IBOutlet var buttonFavoriet: UIButton!
    // UserFavoriteDataのインスタンスを作成
    let user_favorite_data = UserFavoriteData()
    // WebViewに表示するURLを入れる為の変数を宣言
    var detail_url: String!
    // このViewで表示しているCompanyのidを宣言
    var company_id: Int!
    // 今表示しているCompanyがお気に入りかどうか判別するための変数
    var is_favorite: Bool!
    
    // Viewのloadが終わったタイミングで実行される関数
    override func viewDidLoad() {
        super.viewDidLoad()
        is_favorite = user_favorite_data.exist(company_id: company_id)
        // Companyがお気に入りされているかどうかで、お気に入りボタンの表示を変更
        if is_favorite {
            // お気に入りされている場合は、ボタンを"お気に入り解除"に変更する
            buttonFavoriet.setTitle("お気に入り解除", for: .normal)
            
        } else {
            // お気に入りされていない場合は、ボタンを"お気に入りに変更する"
           buttonFavoriet.setTitle("お気に入り", for: .normal)
        }
        
        // webViewにdetail_urlのコンテンツを表示
        if let url = URL(string: self.detail_url) {
            self.webView.load(URLRequest(url: url))
        }
        // Do any additional setup after loading the view.
    }
    
    // 戻るボタンが押された場合の処理
    @IBAction func backView(_ sender: Any) {
        // windowを閉じる
        self.dismiss(animated: true, completion: nil)
    }
    
    // お気に入りボタンが押された場合の処理
    @IBAction func companyFavorite(_ sender: UIButton) {
        // すでにお気に入りされている場合の処理
        if is_favorite {
            // お気に入り削除処理
            user_favorite_data.destroy(company_id: company_id)
            is_favorite = false
            buttonFavoriet.setTitle("お気に入り", for: .normal)
        } else {
            // お気に入り作成処理
            user_favorite_data.create(company_id: company_id)
            is_favorite = true
            buttonFavoriet.setTitle("お気に入り解除", for: .normal)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
