//
//  UserFavoriteData.swift
//  planb_solutions
//
//  Created by 大島勇人 on 2019/01/15.
//  Copyright © 2019 Yuto Oshima. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import UIKit

class UserFavoriteData {
    
    // uuidに紐づいているuserfavoriteを配列で返す
    func index() -> [UserFavorite]{
        // userfavorite返却用の配列を定義する
        var event = [UserFavorite]()
        // UUIDの取得
        let uuid = UIDevice.current.identifierForVendor!.uuidString
        // parameterを指定
        let parameters: Parameters = ["mobile_id": uuid]
        // API(Alamofire)を初期化
        //[:path]が必須
        let api = ApiManager(path: "/user_favorites/index", method: .post, parameters: parameters)//ApiManagerクラスのpathにletで宣言したパラメータを結合する
        var keepAlive = true//共有リソースの排他制御
        // ApiManagerのrequestメソッドの実行
        // requestが正常に実行された場合はsuccess内の処理が走る
        // 失敗した場合は43行目のfail内の処理が走る
        api.request(success: {
            // API通信して解析したデータをdataというDictionaryで受け取る
            (data: Dictionary) in debugPrint(data)
            // 変数user_favoritesにDictionary型のdataから"user_favorites"というkeyのものを代入
            // 代入が成立する = data["user_favorites"]はnilではない
            if let user_favorites = data["user_favorites"] as? [AnyObject]{
                // user_favoritesをuser_favoriteで展開する
                for user_favorite in user_favorites {
                    // UserFavoriteを初期化してプロパティに値を代入
                    let new_user_favorite = UserFavorite(
                        id: user_favorite["id"] as! Int,
                        company_id: user_favorite["company_id"] as! Int
                    )
                    event.append(new_user_favorite)
                }
            }
            keepAlive = false
        }, fail: {
            //api.raquestが失敗した場合はerrorを出す処理
            (error: Error?) in print(error!)
        })
        let runLoop = RunLoop.current
        while keepAlive &&
            runLoop.run(mode: RunLoop.Mode.default, before: NSDate(timeIntervalSinceNow: 0.1) as Date) {
        }
        //Result型を配列に変換
        return event
    }
    
    
    // 新しく作成するFavoriteクラスのインスタンスを返す
    func create(company_id: Int) -> UserFavorite {
        // UserFavoriteのインスタスを作成
        let user_favorite = UserFavorite()
        // uuidの取得
        let uuid = UIDevice.current.identifierForVendor!.uuidString
        // parameterを指定
        let parameters: Parameters = ["mobile_id": uuid, "company_id": company_id]
        // API(Alamofire)を初期化
        //[:path]が必須
        let api = ApiManager(path: "/user_favorites/create", method: .post, parameters: parameters)//ApiManagerクラスのpathにletで宣言したパラメータを結合する
        var keepAlive = true//共有リソースの排他制御
        // ApiManagerのrequestメソッドの実行
        // requestが正常に実行された場合はsuccess内の処理が走る
        // 失敗した場合は81行目のfail内の処理が走る
        api.request(success: {
            // API通信して解析したデータをdataという変数で受け取る
            (data: Dictionary) in
            // プロパティのに値を代入
            user_favorite.id = data["id"] as! Int
            user_favorite.company_id = data["company_id"] as! Int
            keepAlive = false
        }, fail: {
            //api.raquestが失敗した場合はerrorを出す処理
            (error: Error?) in print(error!)
        })
        let runLoop = RunLoop.current
        while keepAlive &&
            runLoop.run(mode: RunLoop.Mode.default, before: NSDate(timeIntervalSinceNow: 0.1) as Date) {
        }
        // user_favoriteを返す
        return user_favorite
    }
    
    // Company_idで指定したUserのインスタンスに紐づいているuser_favoritesを削除する
    func destroy(company_id: Int) {
        // uuidを取得
        let uuid = UIDevice.current.identifierForVendor!.uuidString
        // parameterを指定
        let parameters: Parameters = ["mobile_id": uuid, "company_id": company_id]
        // API(Alamofire)を初期化
        //[:path]が必須
        let api = ApiManager(path: "/user_favorites/destroy", method: .post, parameters: parameters)//ApiManagerクラスのpathにletで宣言したパラメータを結合する
        var keepAlive = true//共有リソースの排他制御
        // ApiManagerのrequestメソッドの実行
        // requestが正常に実行された場合はsuccess内の処理が走る
        // 失敗した場合は111行目のfail内の処理が走る
        api.request(success: {
            (data: Dictionary) in
            // 削除が成功していれば処理を止める
            keepAlive = false
        }, fail: {
            //api.raquestが失敗した場合はerrorを出す処理
            (error: Error?) in print(error!)
        })
        let runLoop = RunLoop.current
        while keepAlive &&
            runLoop.run(mode: RunLoop.Mode.default, before: NSDate(timeIntervalSinceNow: 0.1) as Date) {
        }
    }
    
    func exist(company_id: Int) -> Bool {
        var existance = false
        // uuidを取得
        let uuid = UIDevice.current.identifierForVendor!.uuidString
        // parameterを指定
        let parameters: Parameters = ["mobile_id": uuid, "company_id": company_id]
        // API(Alamofire)を初期化
        //[:path]が必須
        let api = ApiManager(path: "/user_favorites/exist", method: .post, parameters: parameters)//ApiManagerクラスのpathにletで宣言したパラメータを結合する
        var keepAlive = true//共有リソースの排他制御
        // ApiManagerのrequestメソッドの実行
        // requestが正常に実行された場合はsuccess内の処理が走る
        // 失敗した場合は111行目のfail内の処理が走る
        api.request(success: {
            (data: Dictionary) in
            if data["result"] as! Bool == true {
                existance = true
            }
            // 削除が成功していれば処理を止める
            keepAlive = false
        }, fail: {
            //api.raquestが失敗した場合はerrorを出す処理
            (error: Error?) in print(error!)
        })
        let runLoop = RunLoop.current
        while keepAlive &&
            runLoop.run(mode: RunLoop.Mode.default, before: NSDate(timeIntervalSinceNow: 0.1) as Date) {
        }
        return existance
    }
    
}
