//
//  CompanyData.swift
//  planb_solutions
//
//  Created by 大島勇人 on 2019/01/15.
//  Copyright © 2019 Yuto Oshima. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class CompanyData {
    
    // Companyを配列で返す
    func index() -> [Company]{
        // Company返却用の配列を定義する
        var event = [Company]()
        // API(Alamofire)を初期化
        //[:path]が必須
        let api = ApiManager(path: "/companies/index", method: .post)//ApiManagerクラスのpathにletで宣言したパラメータを結合する
        var keepAlive = true//共有リソースの排他制御
        // ApiManagerのrequestメソッドの実行
        // requestが正常に実行された場合はsuccess内の処理が走る
        // 失敗した場合は48行目のfail内の処理が走る
        api.request(success: {
            // API通信して解析したデータをdataというDictionaryで受け取る
            (data: Dictionary) in
            // 変数companiesにDictionary型のdataから"companies"というkeyのものを代入
            // 代入が成立する = data["companies"]はnilではない
            if let companies = data["companies"] as? [AnyObject]{
                // companiesをcompanyで展開する
                for company in companies {
                    // Comanyを初期化してプロパティに値を代入
                    let new_company = Company(
                        id: company["id"] as! Int,
                        name: company["name"] as! String,
                        category: company["category"] as! String,
                        prefecture: company["prefecture"] as! String,
                        employee: company["employee"] as! String,
                        detail_url: company["detail_url"] as! String
                    )
                    // ”img_url"が空ではないかを確認してから代入処理を行う
                    if !(company["img_url"] is NSNull){
                        new_company.img_url = company["img_url"] as! String
                    }
                    // ”description"が空ではないかを確認してから代入処理を行う
                    if !(company["description"] is NSNull){
                        new_company.description = company["description"] as! String
                    }
                    // 返却用の配列にCompanyを追加
                    event.append(new_company)
                }
            }
            keepAlive = false
        }, fail: {
            //api.raquestが失敗した場合はerrorを出す処理
            (error: Error?) in print(error!)
        })
        let runLoop = RunLoop.current
        while keepAlive &&
            runLoop.run(mode: RunLoop.Mode.default, before: NSDate(timeIntervalSinceNow: 0.1) as Date) {
        }
        //Result型を配列に変換
        return event
    }
    
    // 引数に渡されたattribute("description", "prefecture", "category")にkeywordが含まれる全てのcompanyを配列で返す
    func search(attribute: String, keyword: String) -> [Company]{
        // Company返却用の配列を定義する
        var event = [Company]()
        // parameterを指定
        let parameters: Parameters = ["attribute": attribute, "keyword": keyword]
        // API(Alamofire)を初期化
        //[:path]が必須
        let api = ApiManager(path: "/companies/search", method: .post, parameters: parameters)//ApiManagerクラスのpathにletで宣言したパラメータを結合する
        var keepAlive = true//共有リソースの排他制御
        // ApiManagerのrequestメソッドの実行
        // requestが正常に実行された場合はsuccess内の処理が走る
        // 失敗した場合は103行目のfail内の処理が走る
        api.request(success: {
            // API通信して解析したデータをdataというDictionaryで受け取る
            (data: Dictionary) in debugPrint(data)
            // 変数companiesにDictionary型のdataから"companies"というkeyのものを代入
            // 代入が成立する = data["companies"]はnilではない
            if let companies = data["companies"] as? [AnyObject]{
                // companiesをcompanyで展開する
                for company in companies {
                    // Comanyを初期化してプロパティに値を代入
                    let new_company = Company(
                        id: company["id"] as! Int,
                        name: company["name"] as! String,
                        category: company["category"] as! String,
                        prefecture: company["prefecture"] as! String,
                        employee: company["employee"] as! String,
                        detail_url: company["detail_url"] as! String
                    )
                    // ”img_url"が空ではないかを確認してから代入処理を行う
                    if !(company["img_url"] is NSNull){
                        new_company.img_url = company["img_url"] as! String
                    }
                    // ”description"が空ではないかを確認してから代入処理を行う
                    if !(company["description"] is NSNull){
                        new_company.description = company["description"] as! String
                    }
                    // 返却用の配列にCompanyを追加
                    event.append(new_company)
                }
            }
            keepAlive = false
        }, fail: {
            //api.raquestが失敗した場合はerrorを出す処理
            (error: Error?) in print(error!)
        })
        let runLoop = RunLoop.current
        while keepAlive &&
            runLoop.run(mode: RunLoop.Mode.default, before: NSDate(timeIntervalSinceNow: 0.1) as Date) {
        }
        //Result型を配列に変換
        return event
    }
    
}
