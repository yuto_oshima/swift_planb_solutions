//
//  ApiManeger.swift
//  planb_solutions
//
//  Created by 大島勇人 on 2019/01/15.
//  Copyright © 2019 Yuto Oshima. All rights reserved.
//

import Foundation
import Alamofire
private let host = "http://13.112.220.119" //localhostにアクセスする

struct ApiManager{
    
    let url: String
    let method: HTTPMethod
    let parameters: Parameters
    
    // ApiManegerが呼ばれるときに実行される
    init(path: String, method: HTTPMethod = .post, parameters: Parameters = [:]) {
        url = host + path
        self.method = method
        self.parameters = parameters
    }
    
    func request(success: @escaping (_ data: Dictionary<String, Any>)-> Void, fail: @escaping (_ error: Error?)-> Void) {
        Alamofire.request(url, method: method, parameters: parameters).responseJSON { response in
            if response.result.isSuccess {
                success(response.result.value as! Dictionary)
            }else{
                fail(response.result.error)
            }
        }
    }
}
