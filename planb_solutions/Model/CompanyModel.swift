//
//  CompanyModel.swift
//  planb_solutions
//
//  Created by 大島勇人 on 2019/01/15.
//  Copyright © 2019 Yuto Oshima. All rights reserved.
//

import Foundation

// プロパティを13行目で宣言する
// 受け取るAPIに合わせて宣言しないといけない(今回はRails側で宣言しているプロパティと同義のもの)
class Company {
    var id: Int
    var name: String
    var description: String
    var category: String
    var prefecture: String
    var employee: String
    var detail_url: String
    var img_url: String
    
    // コンストラクタにプロパティを宣言する
    // 引数で型 = ~~で宣言するとそれが初期値として扱われる
    // CompanyModelをインスタンス化する際に初期値として呼ばれる
    init(id: Int, name: String, description: String = "", category: String, prefecture: String, employee: String, detail_url: String, img_url: String = ""){
        self.id = id
        self.name = name
        self.description = description
        self.category = category
        self.prefecture = prefecture
        self.employee = employee
        self.detail_url = detail_url
        self.img_url = img_url
    }

}
