//
//  UserFavoriteModel.swift
//  planb_solutions
//
//  Created by 大島勇人 on 2019/01/15.
//  Copyright © 2019 Yuto Oshima. All rights reserved.
//

import Foundation

// プロパティを14行目で宣言する
// 受け取るAPIに合わせて宣言しないといけない(今回はRails側で宣言しているプロパティと同義のもの)
class UserFavorite {
    var id: Int
    var company_id: Int
    
    // コンストラクタにプロパティを宣言する
    // 引数で型 = ~~で宣言するとそれが初期値として扱われる
    // CompanyModelをインスタンス化する際に初期値として呼ばれる
    init(id: Int = 0, company_id: Int = 0){
        self.id = id
        self.company_id = company_id
    }
}
