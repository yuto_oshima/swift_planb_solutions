//
//  OrginalFavoriteViewController.swift
//  planb_solutions
//
//  Created by 大島勇人 on 2019/01/22.
//  Copyright © 2019 Yuto Oshima. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

// ViewControllerでTableView(UITableViewDelegate, UITableViewDataSource)を使うために必要なものを継承
class OrginalFavoriteViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    // UserDataのインスタンスを作成
    let user_data = UserData()
    // Company型の配列を作成
    var companies = [Company]()
    
    // TableViewのOutletの接続
    @IBOutlet weak var tableView: UITableView!
    
    // Viewのloadが終わったタイミングで実行される関数
    override func viewDidLoad() {
        super.viewDidLoad()
        // companiesにUserがFavoriteしたCompanyを代入
        companies = user_data.favorites()
        // UIRefreshCotrolを初期化する
        let refresh = UIRefreshControl()
        // startDownloadを変数refreshに紐づける
        refresh.addTarget(self, action: #selector(self.startDownload), for: UIControl.Event.valueChanged)
        // tableViewのrefreshControlにrefreshを設定
        self.tableView.refreshControl = refresh
        // TableViewの処理をself("OrginalFavoriteViewController")に委託する
        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    // refreshが呼ばれた時に実行される関数
    @objc func startDownload(){
        // companiesにUserがFavoriteしたCompanyを代入
        companies = user_data.favorites()
        // refreshControlの終了
        self.tableView.refreshControl?.endRefreshing()
        // テーブルを再表示
        self.tableView.reloadData()
    }
    
    // Memoryを使いすぎると呼ばれる
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Memory解放する
        // 例 => Imageをlocalにキャッシュしている場合に消去する
    }
    
    // MARK: - Table view data source
    
    // Groupの数を決める(基本的には1以上)
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    // 各section内にrowを何個置く決める
    // 複数のsectionがあり、それぞれで表示する個数が違う場合はsectionという変数の値を元に処理を条件式で分ける
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        // 表示するcompanyの個数分rowを用意する
        return companies.count
    }
    
    // Cellに対して行う処理
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // storyboardにある、任意のcellをidentifierから取得
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? OriginalFavoriteTableViewCell else {
            fatalError("Could not create SampleCell")
        }
        // labelの中はデフォルトでoptional型なので、".text?"になる
        // 表示したいcompany(companies[indexPath.row])のimg_urlが存在するかどうか
        if companies[indexPath.row].img_url != ""{
            // 存在した場合は、img_urlを元に画像を取得しimgViewに表示する
            cell.imgView.load(url: URL(string: companies[indexPath.row].img_url)!)
        } else {
            // 存在しない場合は、Assetsにある"no_imga"を表示する
            cell.imgView.image = UIImage(named: "no_imga")
        }
        // 表示する最大の列を決める
        cell.company_name.numberOfLines = 2
        // 文字のサイズを横幅に合わせて動的に変える
        cell.company_name.adjustsFontSizeToFitWidth = true
        // 動的に変わる文字サイズの下限倍率
        cell.company_name.minimumScaleFactor = 0.8
        // 会社名をcompany_nameというLabelに表示
        cell.company_name.text? = companies[indexPath.row].name
        // 本社所在地をprefectureというLabelに表示
        cell.prefecture.text? = companies[indexPath.row].prefecture
        // 社員数をemployeeというLabelに表示
        cell.employee.text? = companies[indexPath.row].employee
        // 表示する最大の列を決める
        cell.category.numberOfLines = 2
        // 文字のサイズを横幅に合わせて動的に変える
        cell.category.adjustsFontSizeToFitWidth = true
        // 動的に変わる文字サイズの下限倍率
        cell.category.minimumScaleFactor = 0.8
        // 職種をcategoryというLabelに表示
        cell.category.text? = companies[indexPath.row].category
        
        // cellを返す
        return cell
    }

    // 画面遷移前に呼ばれる関数
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         // tableViewの選択されたrowのindexPathを取得
        if let indexPath = self.tableView.indexPathForSelectedRow{
            // indexPathから選択されたcompanyを取得
            let item = companies[indexPath.row]
            // 遷移先のcontrollerを取得
            let controller = segue.destination as! OriginalDetailViewController
            // 遷移先のプロパティをitemから指定
            controller.detail_url = item.detail_url
            controller.company_id = item.id
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
