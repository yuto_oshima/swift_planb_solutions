//
//  OriginalFavoriteTableViewCell.swift
//  planb_solutions
//
//  Created by 大島勇人 on 2019/01/22.
//  Copyright © 2019 Yuto Oshima. All rights reserved.
//

import UIKit

class OriginalFavoriteTableViewCell: UITableViewCell {

    // Outletと紐づいている
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var company_name: UILabel!
    @IBOutlet weak var prefecture: UILabel!
    @IBOutlet weak var employee: UILabel!
    @IBOutlet weak var category: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // 再利用の時に呼ばれる関数
    override func prepareForReuse() {
        super.prepareForReuse()
        self.imgView.image = UIImage(named: "no_img.png")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
