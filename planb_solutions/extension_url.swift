//
//  extension_url.swift
//  planb_solutions
//
//  Created by 大島勇人 on 2019/01/22.
//  Copyright © 2019 Yuto Oshima. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    func load(url: URL) {
        var keepAlive = true
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                        keepAlive = false
                    }
                }
            }
        }
        let runLoop = RunLoop.current
        while keepAlive &&
            runLoop.run(mode: RunLoop.Mode.default, before: NSDate(timeIntervalSinceNow: 0.1) as Date) {
        }
    }
}
