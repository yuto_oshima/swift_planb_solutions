//
//  CompanyTableViewController.swift
//  planb_solutions
//
//  Created by 大島勇人 on 2019/01/11.
//  Copyright © 2019 Yuto Oshima. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class CompanyViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    private let closeCellHeight: CGFloat = 100
    private let openCellHeight: CGFloat = 310
    private var cellHeights: [CGFloat] = []
    let company_data = CompanyData()
    let user_favorite_data = UserFavoriteData()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBAction func favorite_button(_ sender: UIButton) {
        guard let cell = sender.superview?.superview?.superview?.superview as? CompanyCustomCell else {
            print("guard文")
            return
        }
        let indexpath = tableView.indexPath(for: cell)
        // indexpathがnil出ないことを証明する
        if let indexpath = indexpath {
            let company = companies[indexpath.row]
            //company_idだけの配列
            if user_favorite_ids.contains(company.id) {
                // お気に入り削除処理
                user_favorite_data.destroy(company_id: company.id)
                // company.idにヒットしたindex番号を取得する
                let index = user_favorite_ids.firstIndex(of: company.id)
                user_favorites.remove(at: index!)
                user_favorite_ids.remove(at: index!)
                sender.setTitle("お気に入り", for: .normal)
            } else {
                // お気に入り作成処理
                user_favorites.append(user_favorite_data.create(company_id: company.id))
                user_favorite_ids.append(company.id)
                sender.setTitle("お気に入り解除", for: .normal)
            }
        }
    }
    
    var companies = [Company]()
    var user_favorites = [UserFavorite]()
    var user_favorite_ids = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(company_data.index())
        companies = company_data.index()
        user_favorites = user_favorite_data.index()
        user_favorite_ids = user_favorites.map { $0.company_id }
        cellHeights = Array.init(repeating: closeCellHeight, count: companies.count)// いらない
        tableView.backgroundColor = UIColor.gray// いらない
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return companies.count
    }
    
    // いらない
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath.row]
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CompanyCustomCell
        // labelの中はデフォルトでoptional型なので、".text?"になる
        cell.fv_cp_name.numberOfLines = 2
        cell.fv_cp_name.adjustsFontSizeToFitWidth = true
        cell.fv_cp_name.minimumScaleFactor = 0.8
        cell.fv_cp_name.text? = companies[indexPath.row].name
        cell.fv_cp_prefecture.text? = companies[indexPath.row].prefecture
        if companies[indexPath.row].img_url != ""{
            let url = URL(string: companies[indexPath.row].img_url)!
            cell.fv_company_img.af_setImage(withURL: url)
        }
        cell.cv_cp_name.numberOfLines = 2
        cell.cv_cp_name.adjustsFontSizeToFitWidth = true
        cell.cv_cp_name.minimumScaleFactor = 0.8
        cell.cv_cp_name.text? = companies[indexPath.row].name
        cell.cv_cp_description.numberOfLines = 3
        cell.cv_cp_description.adjustsFontSizeToFitWidth = true
        cell.cv_cp_description.minimumScaleFactor = 0.8
        cell.cv_cp_description.text? = companies[indexPath.row].description
        cell.cv_cp_category.numberOfLines = 2
        cell.cv_cp_category.adjustsFontSizeToFitWidth = true
        cell.cv_cp_category.minimumScaleFactor = 0.5
        cell.cv_cp_category.text? = companies[indexPath.row].category
        cell.cv_cp_prefecture.text? = companies[indexPath.row].prefecture
        cell.cv_cp_employee.text? = companies[indexPath.row].employee
        if user_favorite_ids.contains(companies[indexPath.row].id) {
            cell.cv_favorite_button.setTitle("お気に入り解除", for: .normal)
        } else {
            cell.cv_favorite_button.setTitle("お気に入り", for: .normal)
        }
        return cell
    }
    
    // いらない
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard case let cell as CompanyCustomCell = tableView.cellForRow(at: indexPath) else {
            return
        }
        
        var duration = 0.0
        if cellHeights[indexPath.row] == closeCellHeight { // open cell
            cellHeights[indexPath.row] = openCellHeight
            cell.unfold(true, animated: true, completion: nil)
            duration = 0.5
        } else {// close cell
            cellHeights[indexPath.row] = closeCellHeight
            cell.unfold(false, animated: true, completion: nil)
            duration = 1.1
        }
        
        UIView.animate(withDuration: duration, delay: 0, options: .curveEaseOut, animations: {
            tableView.beginUpdates()
            tableView.endUpdates()
        }, completion: nil)
    }
    
    // いらない
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if case let cell as CompanyCustomCell = cell {
            if cellHeights[indexPath.row] == closeCellHeight {
                cell.unfold(false, animated: false, completion:nil)
            } else {
                cell.unfold(true, animated: false, completion: nil)
            }
        }
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        // キーボードをしまう
        searchBar.resignFirstResponder()
        guard let keyword = searchBar.text else {
            return
        }
        
        // 検索する文字がなければCompany.allをする
        if keyword == ""{
            companies = company_data.index()
        } else {
            if searchBar.selectedScopeButtonIndex == 0 {
                companies = company_data.search(attribute: "description", keyword: keyword)
            } else if searchBar.selectedScopeButtonIndex == 1 {
                companies = company_data.search(attribute: "prefecture", keyword: keyword)
            } else {
                companies = company_data.search(attribute: "category", keyword: keyword)
            }
            
        }
        // いらない
        cellHeights = Array.init(repeating: closeCellHeight, count: companies.count)
        // テーブル再表示
        tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.resignFirstResponder()
       //companies = company_data.index()
       //cellHeights = Array.init(repeating: closeCellHeight, count: companies.count)
        //tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let indexPath = self.tableView.indexPathForSelectedRow{
            let item = companies[indexPath.row].detail_url
            let controller = segue.destination as! DetailViewController
            controller.detail_url = item
        }
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
