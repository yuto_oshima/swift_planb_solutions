//
//  CompanyCustomCell.swift
//  planb_solutions
//
//  Created by 大島勇人 on 2019/01/13.
//  Copyright © 2019 Yuto Oshima. All rights reserved.
//

import UIKit
import FoldingCell

class CompanyCustomCell: FoldingCell {
    
    // storyboardとoutlet接続を行なっている
    // それによって、それらにアクセスができるようになった
    @IBOutlet weak var fv_cp_name: UILabel!
    @IBOutlet weak var fv_cp_prefecture: UILabel!
    @IBOutlet weak var cv_cp_name: UILabel!
    @IBOutlet weak var cv_cp_description: UILabel!
    @IBOutlet weak var cv_cp_category: UILabel!
    @IBOutlet weak var cv_cp_prefecture: UILabel!
    @IBOutlet weak var cv_cp_employee: UILabel!
    @IBOutlet weak var cv_favorite_button: UIButton!
    @IBOutlet weak var fv_company_img: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        backgroundColor = UIColor.clear
    }
    
    // animationの速度を変えている？
    override func animationDuration(_ itemIndex:NSInteger, type:AnimationType)-> TimeInterval {
        let durations = [0.26, 0.2, 0.2]
        return durations[itemIndex]
    }
    
}
