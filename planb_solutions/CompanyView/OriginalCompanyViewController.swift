//
//  OriginalCompanyViewController.swift
//  planb_solutions
//
//  Created by 大島勇人 on 2019/01/22.
//  Copyright © 2019 Yuto Oshima. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

// ViewControllerでTableView(UITableViewDelegate, UITableViewDataSource), Searchbar(UISearchBarDelegate)を使うために必要なものを継承
class OriginalCompanyViewController: UIViewController , UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate{
    // CompanyDataのインスタンスを作成
    let company_data = CompanyData()
    // Company型の配列を作成
    var companies = [Company]()
    // TableViewとSearchBarのOutletの接続
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searcBar: UISearchBar!
    
    // Viewのloadが終わったタイミングで実行される関数
    override func viewDidLoad() {
        super.viewDidLoad()
        // companiesに全てのCompanyを代入
        companies = company_data.index()
        // TableView.SearchBarの処理をself("OriginalCompanyViewController")に委託する
        tableView.delegate = self
        tableView.dataSource = self
        searcBar.delegate = self
        // Do any additional setup after loading the view.
    }
    
    // Memoryを使いすぎると呼ばれる
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Memory解放する
        // 例 => Imageをlocalにキャッシュしている場合に消去する
    }
    
    // MARK: - Table view data source
    
    // Groupの数を決める(基本的には1以上)
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    // 各section内にrowを何個置く決める
    // 複数のsectionがあり、それぞれで表示する個数が違う場合はsectionという変数の値を元に処理を条件式で分ける
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        // 表示するcompanyの個数分rowを用意する
        return companies.count
    }

    // Cellに対して行う処理
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // storyboardにある、任意のcellをidentifierから取得
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! OriginalCompanyTableViewCell
        // labelの中はデフォルトでoptional型なので、".text?"になる
        // 表示する最大の列を決める
        cell.company_name.numberOfLines = 2
        // 文字のサイズを横幅に合わせて動的に変える
        cell.company_name.adjustsFontSizeToFitWidth = true
        // 動的に変わる文字サイズの下限倍率
        cell.company_name.minimumScaleFactor = 0.8
        // 会社名をcompany_nameというLabelに表示
        cell.company_name.text? = companies[indexPath.row].name
        // 本社所在地をprefectureというLabelに表示
        cell.prefecture.text? = companies[indexPath.row].prefecture
        // 社員数をemployeeというLabelに表示
        cell.employee.text? = companies[indexPath.row].employee
        // 表示したいcompany(companies[indexPath.row])のimg_urlが存在するかどうか
        if companies[indexPath.row].img_url != ""{
            // 存在した場合は、img_urlを元に画像を取得しimgViewに表示する
            cell.imgView.load(url: URL(string: companies[indexPath.row].img_url)!)
        } else {
            // 存在しない場合は、Assetsにある"no_imga"を表示する
            cell.imgView.image = UIImage(named: "no_imga")
        }
        // 表示する最大の列を決める
        cell.category.numberOfLines = 2
        // 文字のサイズを横幅に合わせて動的に変える
        cell.category.adjustsFontSizeToFitWidth = true
        // 動的に変わる文字サイズの下限倍率
        cell.category.minimumScaleFactor = 0.8
        // 職種をcategoryというLabelに表示
        cell.category.text? = companies[indexPath.row].category
        
        // cellを返す
        return cell 
    }
    
    // searchBarの検索ボタンがクリックされた場合に呼ばれる
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        // キーボードをしまう
        searchBar.resignFirstResponder()
        // 入力された値をkeywordに代入
        guard let keyword = searchBar.text else {
            return
        }
        
        // 検索する文字がなければCompany.allをする
        if keyword == ""{
            companies = company_data.index()
        } else {
            // 選択されたボタンを元にattributeを変更して検索する
            if searchBar.selectedScopeButtonIndex == 0 {
                // 社名・概要が選択されていた場合、"description"からkeywordで検索
                companies = company_data.search(attribute: "description", keyword: keyword)
            } else if searchBar.selectedScopeButtonIndex == 1 {
                // 都道府県が選択されていた場合、"prefecture"からkeywordで検索
                companies = company_data.search(attribute: "prefecture", keyword: keyword)
            } else {
                // 職種が選択されていた場合、"category"からkeywordで検索
                companies = company_data.search(attribute: "category", keyword: keyword)
            }
            
        }
        // テーブル再表示
        tableView.reloadData()
    }
    
    // searchBarのキャンセルボタンが押された場合の処理
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        // searchBarのtextを空にする
        searchBar.text = ""
        // キーボードをしまう
        searchBar.resignFirstResponder()
        //companies = company_data.index()
        //cellHeights = Array.init(repeating: closeCellHeight, count: companies.count)
        //tableView.reloadData()
    }
    
    // 画面遷移前に呼ばれる関数
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // tableViewの選択されたrowのindexPathを取得
        if let indexPath = self.tableView.indexPathForSelectedRow{
            // indexPathから選択されたcompanyを取得
            let item = companies[indexPath.row]
            // 遷移先のcontrollerを取得
            let controller = segue.destination as! OriginalDetailViewController
            // 遷移先のプロパティをitemから指定
            controller.detail_url = item.detail_url
            controller.company_id = item.id
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
