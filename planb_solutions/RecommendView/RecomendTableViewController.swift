//
//  RecomendTableViewController.swift
//  planb_solutions
//
//  Created by 大島勇人 on 2019/01/11.
//  Copyright © 2019 Yuto Oshima. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class RecomendTableViewController: UITableViewController {
    private let closeCellHeight: CGFloat = 100
    private let openCellHeight: CGFloat = 310
    private var cellHeights: [CGFloat] = []
    var user_favorite_ids = [Int]()
    let user_data = UserData()
    var companies = [Company]()
    let user_favorite_data = UserFavoriteData()
    
    @IBAction func favorite_button(_ sender: UIButton) {
        guard let cell = sender.superview?.superview?.superview?.superview as? RecommendCustomCell else {
            print("guard文")
            return
        }
        let indexpath = tableView.indexPath(for: cell)
        // indexpathがnil出ないことを証明する
        if let indexpath = indexpath {
            let company = companies[indexpath.row]
            //company_idだけの配列
            if user_favorite_ids.contains(company.id) {
                // お気に入り削除処理
                user_favorite_data.destroy(company_id: company.id)
                // company.idにヒットしたindex番号を取得する
                let index = user_favorite_ids.firstIndex(of: company.id)
                user_favorite_ids.remove(at: index!)
                sender.setTitle("お気に入り", for: .normal)
            } else {
                // お気に入り作成処理
                user_favorite_ids.append(user_favorite_data.create(company_id: company.id).company_id)
                sender.setTitle("お気に入り解除", for: .normal)
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        companies = user_data.recommend()
        cellHeights = Array.init(repeating: closeCellHeight, count: companies.count)
        let refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector(self.startDownload), for: UIControl.Event.valueChanged)
        self.refreshControl = refresh
        tableView.backgroundColor = UIColor.gray
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    @objc func startDownload(){
        companies = user_data.recommend()
        user_favorite_ids = []
        cellHeights = Array.init(repeating: closeCellHeight, count: companies.count)
        self.refreshControl?.endRefreshing()
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return companies.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath.row]
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RecommendCustomCell
        // labelの中はデフォルトでoptional型なので、".text?"になる
        cell.fv_cp_name.numberOfLines = 2
        cell.fv_cp_name.adjustsFontSizeToFitWidth = true
        cell.fv_cp_name.minimumScaleFactor = 0.8
        cell.fv_cp_name.text? = companies[indexPath.row].name
        cell.fv_cp_prefecture.text? = companies[indexPath.row].prefecture
        if companies[indexPath.row].img_url != ""{
            let url = URL(string: companies[indexPath.row].img_url)!
            cell.fv_company_img.af_setImage(withURL: url)
        }
        cell.cv_cp_name.numberOfLines = 2
        cell.cv_cp_name.adjustsFontSizeToFitWidth = true
        cell.cv_cp_name.minimumScaleFactor = 0.8
        cell.cv_cp_name.text? = companies[indexPath.row].name
        cell.cv_cp_description.numberOfLines = 3
        cell.cv_cp_description.adjustsFontSizeToFitWidth = true
        cell.cv_cp_description.minimumScaleFactor = 0.8
        cell.cv_cp_description.text? = companies[indexPath.row].description
        cell.cv_cp_category.numberOfLines = 2
        cell.cv_cp_category.adjustsFontSizeToFitWidth = true
        cell.cv_cp_category.minimumScaleFactor = 0.5
        cell.cv_cp_category.text? = companies[indexPath.row].category
        cell.cv_cp_prefecture.text? = companies[indexPath.row].prefecture
        cell.cv_cp_employee.text? = companies[indexPath.row].employee
        if user_favorite_ids.contains(companies[indexPath.row].id) {
            cell.cv_favorite_button.setTitle("お気に入り解除", for: .normal)
        } else {
            cell.cv_favorite_button.setTitle("お気に入り", for: .normal)
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard case let cell as RecommendCustomCell = tableView.cellForRow(at: indexPath) else {
            return
        }
        
        var duration = 0.0
        if cellHeights[indexPath.row] == closeCellHeight { // open cell
            cellHeights[indexPath.row] = openCellHeight
            cell.unfold(true, animated: true, completion: nil)
            duration = 0.5
        } else {// close cell
            cellHeights[indexPath.row] = closeCellHeight
            cell.unfold(false, animated: true, completion: nil)
            duration = 1.1
        }
        
        UIView.animate(withDuration: duration, delay: 0, options: .curveEaseOut, animations: {
            tableView.beginUpdates()
            tableView.endUpdates()
        }, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if case let cell as RecommendCustomCell = cell {
            if cellHeights[indexPath.row] == closeCellHeight {
                cell.unfold(false, animated: false, completion:nil)
            } else {
                cell.unfold(true, animated: false, completion: nil)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // indexPathを初期化
        let indexPath :NSIndexPath
        // sender = segueを実際に呼んでいるもの(今回はbuttonからsegueをつなげたので、UIButtonになる)
        if let button = sender as? UIButton {
            // ButtonからUITableViewまでの場所を探している
            let cell = button.superview?.superview?.superview?.superview as! UITableViewCell
            // 初期化したindexPathに上で見つけたCellを元にTableViewからIndexPathを探す
            indexPath = self.tableView.indexPath(for: cell)! as NSIndexPath
            let item = companies[indexPath.row].detail_url
            let controller = segue.destination as! DetailViewController
            controller.detail_url = item
        }
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
