//
//  OriginalRecommendViewController.swift
//  planb_solutions
//
//  Created by 大島勇人 on 2019/01/22.
//  Copyright © 2019 Yuto Oshima. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class OriginalRecommendViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    let user_data = UserData()
    var companies = [Company]()
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        companies = user_data.recommend()
        let refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector(self.startDownload), for: UIControl.Event.valueChanged)
        self.tableView.refreshControl = refresh
        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    @objc func startDownload(){
        companies = user_data.recommend()
        self.tableView.refreshControl?.endRefreshing()
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return companies.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! OriginalRecommendTableViewCell
        // labelの中はデフォルトでoptional型なので、".text?"になる
        if companies[indexPath.row].img_url != ""{
            cell.imgView.load(url: URL(string: companies[indexPath.row].img_url)!)
            print("\(companies[indexPath.row].img_url),\(indexPath)")
        } else {
             cell.imgView.image = UIImage(named: "no_imga")
        }
        cell.company_name.numberOfLines = 2
        cell.company_name.adjustsFontSizeToFitWidth = true
        cell.company_name.minimumScaleFactor = 0.8
        cell.company_name.text? = companies[indexPath.row].name
        cell.prefecture.text? = companies[indexPath.row].prefecture
        cell.prefecture.text? = companies[indexPath.row].prefecture
        cell.employee.text? = companies[indexPath.row].employee
        cell.category.numberOfLines = 2
        cell.category.adjustsFontSizeToFitWidth = true
        cell.category.minimumScaleFactor = 0.8
        cell.category.text? = companies[indexPath.row].category
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let indexPath = self.tableView.indexPathForSelectedRow{
            let item = companies[indexPath.row]
            let controller = segue.destination as! OriginalDetailViewController
            controller.detail_url = item.detail_url
            controller.company_id = item.id
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
