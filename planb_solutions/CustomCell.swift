//
//  CustomCell.swift
//  planb_solutions
//
//  Created by 大島勇人 on 2019/01/11.
//  Copyright © 2019 Yuto Oshima. All rights reserved.
//

import UIKit
import FoldingCell

class CustomCell: FoldingCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        backgroundColor = UIColor.clear
    }

    override func animationDuration(_ itemIndex:NSInteger, type:AnimationType)-> TimeInterval {
        let durations = [0.26, 0.2, 0.2]
        return durations[itemIndex]
    }

}
