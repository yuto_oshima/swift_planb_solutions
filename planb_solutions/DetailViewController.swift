//
//  DetailViewController.swift
//  planb_solutions
//
//  Created by 大島勇人 on 2019/01/16.
//  Copyright © 2019 Yuto Oshima. All rights reserved.
//

import UIKit
import WebKit

class DetailViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    var detail_url: String!
    override func viewDidLoad() {
        super.viewDidLoad()
        if let url = URL(string: self.detail_url) {
            self.webView.load(URLRequest(url: url))
        }
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
